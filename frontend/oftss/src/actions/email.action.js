import axios from "axios";

export const registerEmail = (email) => {
    //console.log(email)
    const registerEmailPromise =axios.post(`${process.env.REACT_APP_API_URL}/sendMail1`,email)

    return {
        payload:registerEmailPromise
    }

}

export const shippingEmail =(email) => {
    const shippingEmailPromise =axios.post(`${process.env.REACT_APP_API_URL}/sendShippingMail`,email)

    return {
        payload:shippingEmailPromise
    }
}

export const forgotPasswordEmail = (email) => {
    console.log(email)
    const forgotPasswordEmailPromise =axios.post(`${process.env.REACT_APP_API_URL}/sendMail2`,email)
        .then(res=>{
            console.log(res);
            return {
                success:true
            }
        }).catch(err=>{
            console.log(err);
            return {
                success:false
            }
        })

    return {
        payload:forgotPasswordEmailPromise
    }

}
