import axios from "axios";
import {appConstants} from "../constants/constants";

export const placeOrder=(order) =>{
    const  placeOrderPromise = axios.post(`${process.env.REACT_APP_API_URL}/orders`,order)
    .then (res => {
        //console.log(res);
        if(res) {
            return {
                success:true,
                order:res.data
            }
        }
        return {
            success:false,
            order:null
        }
    })
    return {
        type: appConstants.PLACE_ORDER,
        payload: placeOrderPromise,
    };
}

export const getOrderByUserId = (userId) => {
    const getOrderByUserIdPromise =axios.get(`${process.env.REACT_APP_API_URL}/orders/userId/${userId}`)
        .then (res => {
            //console.log(res);
            if(res) {
                //console.log(res);
                const needed=[];
                res.data.forEach(e => {
                    // console.log(e.purchases);
                    // console.log(e.purchaseDate);
                    const obj={
                        "order_id": e.id,
                        "purchase_date":e.purchaseDate,
                        "purchases": e.purchases,
                        "status": e.status,
                    }
                    needed.push(obj);
                });
                //console.log(needed);
                return {
                    success:true,
                    orders:needed,
                }
            }
            return {
                success:true,
                orders:null,
            }

        }).catch(err => {
            return {
                success:true,
                orders:null,
            }
        })

    return {
        type:appConstants.GET_ORDER_BY_USERID,
        payload: getOrderByUserIdPromise,
    };
}

export const getAllOrders = () => {
    const getAllOrdersPromise =axios.get(`${process.env.REACT_APP_API_URL}/orders`)
        .then ( res => {
            //console.log(res);
            if(res) {
                //console.log(res);
                const needed=[];
                res.data.forEach(e => {
                    // console.log(e.purchases);
                    // console.log(e.purchaseDate);
                    const obj={
                        "order_id" : e.id,
                        "purchase_date":e.purchaseDate,
                        "purchases": e.purchases,
                        "status":e.status,
                    }
                    needed.push(obj);
                });
                //console.log(needed);
                return {
                    success:true,
                    orders:needed,
                }
            }
            return {
                success:true,
                orders:null,
            }

        }).catch(err => {
            return {
                success:true,
                orders:null,
            }
        })

    return {
        type:appConstants.GET_ORDER_BY_USERID,
        payload: getAllOrdersPromise,
    };

}

// export const deleteOrderById = (id) => {
//     const deleteOrderByIdPromise=axios.delete(`${process.env.REACT_APP_API_URL}/orders/${id}`)
//         .then (res => {
//                 //console.log(res);
//                 if(res.success) {
//                     return {
//                         deleted: true,
//                     }
//                 }
//
//                 return {
//                     deleted:false,
//                 }
//         })
//         .catch(err => {
//             return {
//                 deleted:false,
//             }
//         })
//
//     return {
//         type:appConstants.GET_ORDER_BY_USERID,
//         payload: deleteOrderByIdPromise,
//     };
// }


export const getShippingInfo = () => {
    const getShippingInfoPromise =axios.get(`${process.env.REACT_APP_API_URL}/orders`)
        .then (res=> {
            const needed=[]
            res.data.forEach( e=> {
                    const obj={
                        "order_Id": e.id,
                        "purchase_date": e.purchaseDate,
                        "shipping_address": e.user.userDetail,
                        "purchases": e.purchases,
                        "status":e.status,
                    }
                    needed.push(obj);
                }
            )

            return {
                success:true,
                shippingInfo:needed,
            }
        })
        return {
            type:appConstants.GET_SHIPPING_INFO,
            payload: getShippingInfoPromise,
        };
}

export const getAllPurchases = ()=> {
    const getAllPurchasesPromise =axios.get(`${process.env.REACT_APP_API_URL}/orders`)
        .then(res => {
            //console.log(res);
            const needed=[];
            res.data.forEach(e => {
                const obj={
                    "purchase_date":e.purchaseDate,
                    "purchases": e.purchases,
                }
                needed.push(obj);
            });
            //console.log(needed);
            return {
                success:true,
                orders:needed,
            }
        })
    return {
        type:appConstants.GET_ALL_PURCHASES,
        payload: getAllPurchasesPromise,
    };
}

export const updateOrder =(order) => {
    //console.log(order);
    const updateOrderPromise=axios.put(`${process.env.REACT_APP_API_URL}/orders`,order)
        .then(res=> {
            //console.log(res);
            return{
                success:true,
            }
        })
    return {
        payload: updateOrderPromise
    }
}

export const cancelOrders = (cancelledOrdersId) => {
    let symbol="";
    symbol=cancelledOrdersId.toString();
    console.log(symbol);
    const addressList = ["yinan.zhou2015@gmail.com"];
    const cancelOrdersPromise = axios.post(`${process.env.REACT_APP_API_URL}/sendCancelMail/${symbol}`,addressList)
        .then ( res => {
            return {
                success:true,
            }
        })
        .catch(err => {
            console.log(cancelledOrdersId)
            return {
                success:false,
            }
        });
    return {
        payload:cancelOrdersPromise,
    }
}
