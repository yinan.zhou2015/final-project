import axios from "axios";
import {appConstants} from "../constants/constants";

export const getProducts = () => {
    const  getProductsPromise = axios.get(`${process.env.REACT_APP_API_URL}/products`)

    return {
        type: appConstants.GET_PRODUCTS,
        payload: getProductsPromise,
    };
};

export const getStockCheckData = () => {
    let data=[];

    const handleResponse= (res) => {
        //console.log(res);
        let platform=[];
        platform =res.map(e=> e.platform);
        const unique = [...new Set(platform)];
        let stocks =[];
        unique.forEach(platform => {
            let total=0;
            const ele= {
                name: '',
                stock: 0,
            }
            res.forEach(e => {
                if(e.platform===platform) {
                    total +=e.stock;
                }
            })
            //console.log(platform);
            ele.name=platform;
            //console.log(total);
            ele.stock=total;
            stocks.push(total);
            data.push(ele);
        })

        return data;
    };

    const getStockCheckDataPromise = axios.get(`${process.env.REACT_APP_API_URL}/products`)
        .then(res => {
            const handledData=handleResponse(res.data);
            console.log(handledData);
            return {
                success:true,
                data:handledData,
            }
        });
    return {
        type: appConstants.GET_STOCK_TO_CHECK,
        payload: getStockCheckDataPromise,
    };
}

export const addProduct = (product) => {
    const addProductPromise = axios.post(`${process.env.REACT_APP_API_URL}/products`,product)
        .then(res => {
            console.log(res);
            if (res.success){
                return {
                    success: true,
                    product
                }
            }
            return {
                success: false,
                product:null,
            }
        })
        .catch(err => {
            return {
                success:false,
                product:null
            };
        });

    return {
        type: appConstants.ADD_PRODUCT,
        payload: addProductPromise,
    }
}


export const editProduct = (product) => {
    const editProductPromise = axios.put(`${process.env.REACT_APP_API_URL}/products`,product)
        .then(res => {
            // console.log(res)
            if (res ){
                // console.log('edit: succesffully')
                return {
                    success: true,
                }
            }
            //console.log('edit:',res)
            return {
                success: false,
            }
        })
        .catch(err => {
            console.log('edit error:',false)
            return {
                success:false,
            };
        });

    return {
        type: appConstants.EDIT_PRODUCT,
        payload: editProductPromise,
    }
}
