import React, {useState} from "react";
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import {addProduct} from "../actions/products.action";
import {showSuccess} from "../components/Alert";
import 'bootstrap/dist/css/bootstrap.min.css';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));


let url=null;
export const AddNewGame =()=> {

    const classes = useStyles();
    const [info,setInfo]=useState({
        name:'',
        category:'',
        platform:'',
        price:'',
        stock:'',
        image:'',
    });


    const [success,setSuccess]=useState(false);

    const fileUploadHandler= event => {
        const formData = new FormData();
        const fd=event.target.files[0];
        console.log(fd)
        formData.append( 'file',fd );
        // fd.append('image',info.selectedFile,info.selectedFile.name);
        axios.post(`${process.env.REACT_APP_API_URL}/storage/uploadFile`,formData, {
            onUploadProgress:progressEvent => {
                console.log('upload progress: '+
                    ((progressEvent.loaded/progressEvent.total) * 100) +'%')
            }
        })
            .then(res => {
                //console.log(res.data);
                const urlArr=res.data.split('/')
                url="http://finalproject123.s3-website.us-east-2.amazonaws.com/".concat(urlArr[urlArr.length-1]);
                console.log(url);
                setInfo({...info,image:url});
            })
    }


    const handleChange=(event)=> {
        const newInfo={...info};
        newInfo[event.target.id]=event.target.value;

        setInfo(newInfo);
    }

    const handleSubmit= (event) => {
        event.preventDefault();
        addProduct(info);
        setSuccess(true);
    }

    return (
        <div className="d-flex justify-content-center">
            <form className={classes.root}noValidate autoComplete="off"
            >
                <div className="form-group">
                    <h2 className={classes.header}>Add Game</h2>
                </div>
                {showSuccess({
                    success:success,
                    message: "Successfully Added a New Game Please Check Your Storage !",
                })}
                <div className="form-group">
                    <TextField id="name" type="text" label="Name" variant="filled"
                               onChange={handleChange}
                    />
                </div>

                <div className="form-group">
                    <TextField id="category" type="text" label="Category" variant="filled"
                               onChange={handleChange}
                    />
                </div>

                <div className="form-group">
                    <TextField id="platform" type="text" label="Platform" variant="filled"
                               onChange={handleChange}
                    />
                </div>

                <div className="form-group">
                    <TextField id="price" type="number" label="Price" variant="filled"
                               onChange={handleChange}
                    />
                </div>

                <div className="form-group">
                    <TextField id="stock" type="number" label="Stock" variant="filled"
                               onChange={handleChange}
                    />
                </div>

                <div className="form-group">
                    <TextField id="image" type="file"  variant="filled"
                               onChange={fileUploadHandler}
                    />
                </div>
                <div className="form-group">
                    <button type="button" className="btn btn-primary" onClick={handleSubmit}>Add Game</button>
                </div>
            </form>
        </div>

    );
}
