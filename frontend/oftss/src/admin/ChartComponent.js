import React, { PureComponent } from 'react';
import {
    BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';
import {connect} from "react-redux";
import {getStockCheckData} from "../actions/products.action";


class ChartComponent extends PureComponent {


    componentDidMount() {
        // optimize it send request when needed
        this.props.getStockCheckData();
    }


    render() {
        //console.log(this.props.data);
        return (
            <BarChart
                width={800}
                height={700}
                data={this.props.data}
                margin={{
                    top: 5, right: 30, left: 20, bottom: 5,
                }}
                barSize={20}
            >
                <XAxis dataKey="name" scale="point" padding={{ left: 10, right: 10 }} />
                <YAxis />
                <Tooltip />
                <Legend />
                <CartesianGrid strokeDasharray="3 3" />
                <Bar dataKey="stock" fill="#8884d8" background={{ fill: '#eee' }} />
            </BarChart>
        );
    }
}

function mapStateToProps({products}) {
    console.log(products);
    return {data:products};
}

export default connect(mapStateToProps, {getStockCheckData}) (ChartComponent);





