import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {getAllOrders, updateOrder} from "../actions/order.action";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Pagination} from "../components/Pagination";

export const OrdersHistory =() => {

    const [history,setHistory] = useState({
            purchases:[],
            error: false,
            success:true,
            delete: false,
            start:0,
            end:4,
        }
    );
    const dispatch=useDispatch();

    const historyPromise=dispatch(getAllOrders());
    historyPromise.then(res => {
        //console.log(res);
        if(history.success){
            //console.log(res.payload.orders)
            setHistory({...history,purchases:res.payload.orders,success: false});
            // console.log(history)
        }

    });

    const handleCancel =(e) => {
        //console.log(e.target.value)
        const order ={
            id:e.target.value,
            status:"cancelled"
        }

        const newPurchases=[];
        history.purchases.forEach(purchase=>{
            if(purchase.order_id==e.target.value) {
                purchase.status=order.status;
            }
            newPurchases.push(purchase);
        })
        setHistory({...history,purchases:newPurchases});
        updateOrder(order);
        //deleteOrderById(e.target.value);
    }

    const itemTotal=(price,qty)=>{
        return price*qty;
    }

    const paginate=(pageNumber) => {
        setHistory({...history,start:(pageNumber-1)*4 ,end:pageNumber*4});
    }

    const orderHistory = history => {
        return (
            <div className="card mb-5">
                <h3 className="card-header">Orders history</h3>
                {/*{showSuccess({*/}
                {/*    success:history.delete,*/}
                {/*    message: "Successfully cancelled an order",*/}
                {/*})}*/}
                <ul className="list-group">
                    <li className="list-group-item">
                        {history.purchases.slice(history.start,history.end).map((h, i) => {
                            //console.log(h.order_id)
                            return (
                                <div key={i}>
                                    <h6>Order Id: {h.order_id}</h6>
                                    {h.purchases.map((p, i) => {
                                        return (
                                            <div key={i}>
                                                <img src={p.product.image} style={{
                                                    height:'50px',
                                                    width:'50px',
                                                }}/>
                                                <h6>Item name: {p.product.name}</h6>
                                                <h6>Qty: {p.qty}</h6>
                                                <h6>
                                                    Item price: ${itemTotal(p.product.price,p.qty)}
                                                </h6>
                                                <h6>
                                                    Purchased date: {h.purchase_date}
                                                </h6>
                                            </div>
                                        );
                                    })}
                                    <h6>Status: {h.status}</h6>
                                    <button
                                        value = {h.order_id}
                                        onClick={(event) => handleCancel(event)}
                                        disabled={h.status!=="processing"}
                                    >Cancel
                                    </button>
                                    <hr />
                                </div>

                            );
                        })}
                    </li>
                </ul>
            </div>
        );
    };
    return (
        <div>
            {orderHistory(history)}
            <Pagination postPerPage={4} totalPosts={history.purchases.length} paginate={paginate}/>
        </div>
    );
}
