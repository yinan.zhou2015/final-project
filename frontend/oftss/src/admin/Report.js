import React, { Component } from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import {connect} from "react-redux";
import {getAllPurchases} from "../actions/order.action";

class Report extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columnDefs: [{
                headerName: "Product", field: "make"
            }, {
                headerName: "QTY", field: "model"
            }, {
                headerName: "Price", field: "price"
            },{
                headerName: "total", field: "total"
            }
            ],
            rowData: [{
                make: "Toyota", model: "Celica", price: 35000,total:35000,
            }, {
                make: "Ford", model: "Mondeo", price: 32000,total:32000,
            }, {
                make: "Porsche", model: "Boxter", price: 72000,total:72000,
            }]
        }
    }

    componentDidMount() {
        !this.props.purchases && this.props.getAllPurchases();
    }

    render() {
        console.log(this.props.purchases)
        return (
            <div
                className="ag-theme-balham"
                style={{
                    height: '500px',
                    width: '1000px', }}
            >
                <AgGridReact
                    columnDefs={this.state.columnDefs}
                    rowData={this.state.rowData}>
                </AgGridReact>
            </div>
        );
    }
}

function mapStateToProps({orders}) {
    //console.log(orders);
    return {purchases:orders};
}

export default connect(mapStateToProps,{getAllPurchases}) (Report);
