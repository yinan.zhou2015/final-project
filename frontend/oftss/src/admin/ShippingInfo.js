import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {getShippingInfo, updateOrder} from "../actions/order.action";
import {shippingEmail} from "../actions/email.action";
import {ControlledExpansionPanel} from "../components/ControlledExpansionPanels";
import {Pagination} from "../components/Pagination";
import {CustomSelector} from "../components/CustomSelector";


export const ShippingInfo  = ()=> {

    const [data,setData]=useState({
        tableInfo:[],
        originalTableInfo:[],
        success:true,
        error:false,
        start:0,
        end:4,
    });

    const dispatch=useDispatch();
    const shippingInfo=dispatch(getShippingInfo());

    const handleShipping=(event)=>{
        // console.log(event.target.id);
        // console.log(event.target.value);
        //console.log(event.target.name);
        const addressList=[];
        addressList.push(event.target.name);
        const order={
                id:event.target.id,
                status:'shipped',
            }
        const newTableInfo=[];
        data.tableInfo.forEach(purchase=>{
            if(purchase.order_Id==event.target.id) {
                purchase.status=order.status;
            }
            newTableInfo.push(purchase);
        })

        updateOrder(order);
        shippingEmail(addressList);

        setData({...data,tableInfo:newTableInfo});
    }


    shippingInfo.then(res=>{
        if(data.success) {
            const newData=[];
            //console.log(res.payload.shippingInfo);
            res.payload.shippingInfo.forEach(e=>
            {
                const orderInfo={
                    order_Id:'',
                    purchase_date:'',
                    name:'',
                    email:'',
                    address:'',
                    purchases:'',
                    status:'',
                }
                orderInfo.order_Id=e.order_Id;
                orderInfo.purchase_date=e.purchase_date;
                orderInfo.name=e.shipping_address.name;
                orderInfo.email=e.shipping_address.email;
                orderInfo.address=''.concat(e.shipping_address.address1,
                                            ",",
                                            e.shipping_address.address2,
                                            ",",
                                            e.shipping_address.city,
                                            ",",
                                            e.shipping_address.state,
                                            ",",
                                            e.shipping_address.zip,
                                        );

                const obj= {
                    status: e.status,
                    purchases: e.purchases,
                }
                // const newPurchases=[];
                // newPurchases.push(obj);
                orderInfo.purchases=obj;
                orderInfo.status=e.status;
                newData.push(orderInfo)
                //console.log(orderInfo);
            })
            setData({...data,tableInfo:newData,originalTableInfo:newData,success:false,error:false});
        }
    })

    const handleCancel=(event)=> {

        const order ={
            id:event.target.id,
            status:"cancelled"
        }
        updateOrder(order);
        const newTableInfo=[];
        data.tableInfo.forEach(purchase=>{
            if(purchase.order_Id==event.target.id) {
                purchase.status=order.status;
            }
            newTableInfo.push(purchase);
        })
        setData({...data,tableInfo:newTableInfo});
    }

    const handleClick=(event)=>{
        console.log(event.target.value);
        if(event.target.value==="pending"){
            handleShipping(event);
        }
        if(event.target.value==="processing") {
            handleCancel(event);
        }
    }

    const handleChange=(status)=>{
        //console.log(status)
        const newDataTableInfo= data.originalTableInfo.filter(e=> {
            return status==='other'? e:e.status===status
        });
        //console.log(newDataTableInfo);
        setData({...data,tableInfo:newDataTableInfo});
        //setStatus()
    }

    const renderName=(status)=>{
        if(status==='pending')
        {
            return 'Ship';
        }else if(status==="processing"){
            return 'Cancel';
        } else if(status==="shipped"){
            return 'Shipped';
        }
        else{
            return 'Cancelled'
        }
    }


    const renderTable=()=> {
        //console.log(data.tableInfo);
        return (
            <table className="table">
                <thead className="thead-dark">
                <tr>
                    <th scope="col">Order_Id</th>
                    <th scope="col">Purchase Date</th>
                    <th scope="col">Name</th>
                    <th scope="col">Address</th>
                    <th scope="col">
                        <CustomSelector selectStatus={handleChange}/>
                    </th>
                    <th scope="col">Purchases</th>

                </tr>
                </thead>

                <tbody>
                {data.tableInfo.slice(data.start,data.end).map(row=> {
                    //console.log(row.email);
                    return(
                        <tr key={row.order_Id}>
                            <td>{row.order_Id}</td>
                            <td>{row.purchase_date}</td>
                            <td>{row.name}</td>
                            <td>{row.address}</td>
                            <td>
                                <button
                                    id={row.order_Id}
                                    value={row.status}
                                    name={row.email}
                                    disabled={row.status==='cancelled' ||row.status==='shipped'}
                                    onClick={handleClick}
                                >
                                    {renderName(row.status)}
                                </button>
                            </td>
                            <td>
                                <ControlledExpansionPanel purchases={row.purchases} />
                            </td>
                        </tr>
                    );
                })}
                </tbody>

            </table>
        );
    }

    const paginate=(pageNumber)=>{
        setData({...data,start:(pageNumber-1)*4 ,end:pageNumber*4});
    }

    return(
        <div>
            {renderTable()}
            <Pagination postPerPage={4} totalPosts={data.tableInfo.length} paginate={paginate}/>
            {/*<AgGrid data={data.tableInfo}/>*/}
        </div>
    )
}
