import React, {useState} from "react";
import {forgotPasswordEmail} from "../actions/email.action";
import Modal from "react-bootstrap/Modal";
import 'bootstrap/dist/css/bootstrap.min.css';

export function Example() {
    let e1 = 0;
    const [show, setShow] = useState(false);
    const handleClose = () => {
        //console.log(e1)
        const addressList = [];
        addressList.push(e1)
        //console.log(addressList)
        forgotPasswordEmail(addressList)


    };
    const handleOnclose = () =>{
        setShow(false)
    }
    const handleShow = () => {
        setShow(true)
    };
    const handleOnChange = (e) =>{
        //console.log(e.target.value)
        e1 = e.target.value;
    }

    return (
        <>
            <button variant="body2" className="text-info"
               onClick={handleShow}>
                Forget password
            </button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>forget password</Modal.Title>
                </Modal.Header>
                <Modal.Body>You forget your password, Enter your email below!
                    <label style={{display:"block"}}><strong>Email:</strong></label>

                    <input placeholder="Email" onChange={(event) => {handleOnChange(event)}}/>
                </Modal.Body>

                <Modal.Footer>
                    <button variant="secondary" onClick={handleOnclose}>
                        Close
                    </button>
                    <button variant="primary" onClick={handleClose}>
                        Done
                    </button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
