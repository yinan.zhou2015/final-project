import React, {useState} from "react";
import {AgGridReact} from "ag-grid-react";
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';

export const AgGrid =({data})=> {
    console.log(data);
    const [columnDefs]=useState([
        { headerName: "Order Id", field: "order_Id" },
        { headerName: "Purchase Date", field: "purchase_date" },
        { headerName: "Name", field: "name" },
        // { headerName: "Address", field: "model" },
        //
        // { headerName: "Name", field: "price",sortable: true, filter: true}
        ]);
    const [rowData] =useState([]);

    const formatData=(dataBefore)=> {
        const dataAfter =[];
        dataBefore.forEach(e => {
            const obj={
                order_Id:e.order_Id,
                purchase_date:e.purchase_date,
                name:e.name,
            }
            // obj.order_Id=;
            // obj.purchase_date=e.purchase_date;
            // obj.name=e.name;
            dataAfter.push(obj);
        })
        //console.log(dataAfter)
        return dataAfter;
    }

    //console.log(rowData.data);
    return (
        <div className="ag-theme-balham" style={ {height: '200px', width: '600px'} }>
            <AgGridReact
                columnDefs={columnDefs}
                rowData={rowData}>
            </AgGridReact>
        </div>
    )
}
