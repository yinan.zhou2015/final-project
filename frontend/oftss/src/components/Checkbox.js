import React, {useState} from "react";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

export const CheckBoxes = () =>{
    const [value, setValue] = React.useState('');

    const handleChange = (event) => {
        console.log(event.target.value)
        setValue(event.target.value);
    };

    return(
        <FormControl component="fieldset">
            <FormLabel component="legend">Gender</FormLabel>
            <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange}>
                <FormControlLabel value="playstation 4" control={<Radio />} label="PS4" />
                <FormControlLabel value="playstation 3" control={<Radio />} label="PS3" />
                <FormControlLabel value="other" control={<Radio />} label="Other" />
                {/*<FormControlLabel value="disabled" disabled control={<Radio />} label="(Disabled option)" />*/}
            </RadioGroup>
        </FormControl>
    )


}

