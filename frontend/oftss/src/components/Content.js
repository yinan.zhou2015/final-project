import React from 'react';
import './Content.scss'
export const Content = () => {
    return (
        <div className="content">
            <div className="mobile-only">I'm a fancy mobile div you can check my turn to next row</div>
            <div className="desktop-only">I'm a heavy desktop div</div>
        </div>
    );
};
