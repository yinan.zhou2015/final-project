import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));
export const CustomSelector =({selectStatus})=> {

    const classes = useStyles();
    const [status, setStatus] = React.useState('');
    return (
        <div>
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">Status</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={status}
                    onChange={(event)=> {
                        selectStatus(event.target.value)
                        setStatus(event.target.value)
                    }}
                >
                    <MenuItem value='shipped'>Shipped</MenuItem>
                    <MenuItem value='processing'>Cancel</MenuItem>
                    <MenuItem value='pending'>Ship</MenuItem>
                    <MenuItem value='cancelled'>Cancelled</MenuItem>
                    <MenuItem value='other'>Other</MenuItem>
                </Select>
            </FormControl>
        </div>
    )

}
