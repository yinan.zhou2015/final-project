import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 140,
    },
});

export const ProductCard =({purchases})=> {
    const classes = useStyles();

    const getTotal= ()=>{
        let total=0.0;
        purchases.purchases.forEach( e=> {
            total +=e.qty * e.product.price;
        })

        return total.toFixed(2);
    }


    return (
        <Card className={classes.root}>
            {
                purchases.purchases.map(pur =>
                    <CardActionArea key={pur.id}>
                        <CardMedia
                            className={classes.media}
                            image={pur.product.image}
                            title={pur.product.name}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {pur.product.name}
                            </Typography>
                            {/*<p>Price: ${p.product.price}</p>*/}
                            {/*<p>Quantity: ${p.qty}</p>*/}
                            <Typography variant="body2" color="textSecondary" component="p">
                                Price: ${pur.product.price}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                Quantity:{pur.qty}
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                )
            }
            <h6>Status:{purchases.status}</h6>
            <h6>Total:$ {getTotal()}</h6>

            {/*<CardActions>*/}
            {/*    <Button size="small" color="primary">*/}
            {/*        Cancel*/}
            {/*    </Button>*/}
            {/*</CardActions>*/}
        </Card>
    );
}
