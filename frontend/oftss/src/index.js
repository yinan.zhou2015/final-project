import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {Redirect, Route, Switch} from "react-router";
import {appConstants} from "./constants/constants";
import App from "./containers/App";
import {BrowserRouter} from "react-router-dom";
import {Home} from "./home/Home.";
import Products from "./products/Products";
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import reduxPromise from 'redux-promise'
import {rootReducer} from "./reducers/root.reducer";
import {Login} from "./auth/Login";
import Signup from "./auth/Signup";
import ProductDetail from "./products/ProductDetail";
import Cart from "./products/cart";
import Dashboard from "./user/UserDashboard";
import Checkout1 from "./products/Checkout1";
import EditProduct from "./admin/EditProduct";
import UpdateProfile from "./user/UpdateProfile";
import AdminDashboard from "./admin/AdminDashboard";
import ChartComponent from "./admin/ChartComponent";
import Report from "./admin/Report";
import {ChangePassword} from "./user/ChangePassword";
import {ShippingInfo} from "./admin/ShippingInfo";
import {OrdersHistory} from "./admin/OrdersHistory";
import {AddNewGame} from "./admin/AddNewGame";
import {Content} from "./components/Content";

ReactDOM.render(
    <Provider store={applyMiddleware(reduxPromise)(createStore)(rootReducer)}>
    <BrowserRouter>
    <App>
        <Switch> {/*Routes in Angular*/}
            <Route path={appConstants.loginRoute} component={Login}/>
            <Route path={appConstants.homeRoute} component={Home}/>
            <Route path={appConstants.addProductRoute} component={AddNewGame} />
            <Route path={appConstants.videoGamesRoute} component={Products}/>

            <Route path={appConstants.registerRoute} component={Signup}/>
            <Route path="/product/:id" exact component={ProductDetail}/>

            <Route path={`${appConstants.editRoute}/:id`} component={EditProduct}/>

            <Route path={appConstants.userProfileRoute} exact component={Dashboard}/>
            <Route path={appConstants.cartRoute} exact component={Cart}/>
            <Route path="/checkout1" exact component={Checkout1}/>
            <Route path={appConstants.updateProfileRoute} exact component={UpdateProfile}/>
            <Route path={appConstants.adminDashboadRoute} component={AdminDashboard}/>
            <Route path={appConstants.chartRoute} component={ChartComponent}/>
            <Route path={appConstants.reportRoute} component={Report}/>
            <Route path={appConstants.changePasswordRoute} component={ChangePassword}/>
            <Route path={appConstants.shippingInfoRoute} component={ShippingInfo}/>
            <Route path={appConstants.ordersHistoryRoute} component={OrdersHistory}/>

            <Route path= "/responsive-content" component={Content}/>

            <Route path ="*">
                <Redirect to={appConstants.homeRoute}/>
            </Route>
        </Switch>
    </App>
    </BrowserRouter>
    </Provider>
    , document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
