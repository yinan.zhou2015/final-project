import React, {useState} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import {updateUserDetail} from "../actions/auth.action";


export default function AddressForm() {

    const userDetail = JSON.parse(localStorage.ud);
    const [addressForm,setAddressForm]=useState({
        name:userDetail.name,
        address1:userDetail.address1,
        address2:userDetail.address2,
        city:userDetail.city,
        state:userDetail.state,
        zip:userDetail.zip,
        country:'US',
    });

    const handleChange= event => {
        //setUserDetail({...userDetail,[name]:event.target.value});
        //userDetail={...userDetail,[name]:event.target.value};
        event.preventDefault();
        const newAddresForm={...addressForm};
        newAddresForm[event.target.id]=event.target.value;
        setAddressForm(newAddresForm);

        const newUserDetail ={
            id:userDetail.id,
            name: newAddresForm.name,
            email:userDetail.email,
            address1:newAddresForm.address1,
            address2:newAddresForm.address2,
            city:newAddresForm.city,
            state:newAddresForm.state,
            zip:newAddresForm.zip,
        }
        updateUserDetail(newUserDetail);
        localStorage.setItem("addressForm",JSON.stringify(addressForm));
        //console.log(addressForm);
    };

    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom>
                Shipping address
            </Typography>
            <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <TextField
                            required
                            id="name"
                            name="name"
                            label="Name"
                            fullWidth
                            defaultValue={addressForm.name}
                            onChange={handleChange}
                            // autoComplete="fname"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            id="address1"
                            name="address1"
                            label="Address line 1"
                            fullWidth
                            defaultValue = {addressForm.address1}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            id="address2"
                            name="address2"
                            label="Address line 2"
                            fullWidth
                            autoComplete="billing address-line2"
                            defaultValue = {addressForm.address2}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="city"
                            name="city"
                            label="City"
                            fullWidth
                            autoComplete="billing address-level2"
                            defaultValue = {addressForm.city}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            id="state"
                            name="state"
                            label="State/Province/Region"
                            fullWidth
                            defaultValue = {addressForm.state}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="zip"
                            name="zip"
                            label="Zip / Postal code"
                            fullWidth
                            autoComplete="billing postal-code"
                            defaultValue = {addressForm.zip}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="country"
                            name="country"
                            label="Country"
                            fullWidth
                            autoComplete="billing country"
                            defaultValue = {addressForm.country}
                        />
                    </Grid>
                    {/*<Grid item xs={12}>*/}
                    {/*    <FormControlLabel*/}
                    {/*        control={<Checkbox color="secondary" name="saveAddress" value="yes" />}*/}
                    {/*        label="Use this address for payment details"*/}
                    {/*    />*/}
                    {/*</Grid>*/}
                </Grid>

        </React.Fragment>
    );
}
