import React, { useState} from "react";

import { Link } from "react-router-dom";
import "braintree-web";

const Checkout = ({ products }) => {
    const [data, setData] = useState({
        loading: false,
        success: false,
        clientToken: null,
        error: "",
        instance: {},
        address: ""
    });

    const getTotal = () => {
        return products.reduce((currentValue, nextValue) => {
            return currentValue + nextValue.count * nextValue.price;
        }, 0).toFixed(2);
    };

    const showCheckout = () => {
       if(localStorage.user == null)  {
           return (
           <Link to="/login">
               <button className="btn btn-primary">Sign in to checkout</button>
           </Link>
           )
       }
       else {
           if(getTotal()!=='0.00') {
               return (
                   <Link to="/checkout1">
                       <button className="btn btn-primary">Check Out</button>
                   </Link>
               )
           }
       }

    };



    const showError = error => (
        <div
            className="alert alert-danger"
            style={{ display: error ? "" : "none" }}
        >
            {error}
        </div>
    );

    const showSuccess = success => (
        <div
            className="alert alert-info"
            style={{ display: success ? "" : "none" }}
        >
            Thanks! Your payment was successful!
        </div>
    );

    const showLoading = loading =>
        loading && <h2 className="text-danger">Loading...</h2>;

    return (
        <div>
            <h2>Total: ${getTotal()}</h2>
            {showLoading(data.loading)}
            {showSuccess(data.success)}
            {showError(data.error)}
            {showCheckout()}
        </div>
    );
};

export default Checkout;
