import React, {useState} from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

export default function PaymentForm() {

    const [payment, setPayment] = useState({
        cardName: '',
        cardNumber: '',
        expDate: '',
        cvv: '',
    });

    const [validation, setValidation] = useState({
        // invalidName:true,
        invalidCardNumber: true,
        // invalidExpDate:true,
        // invalidCvv:true,
    })

    const handleChange = (event) => {
        event.preventDefault();
        let newPayment = {...payment};
        newPayment[event.target.id] = event.target.value;
        setPayment(newPayment);
        // checkInput(event.target.value, event.target.id);
        // console.log(validation);
        // console.log(payment);
        if(event.target.id==="cardNumber"){
            checkCardNumber(event.target.value,event.target.id);
        }
        localStorage.setItem("payment", JSON.stringify(payment));
    }

    const checkCardNumber = (value,target)=> {
        if (target === 'cardNumber' && value
            .match('^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\\d{3})\\d{11})$')) {
            setValidation({...validation, invalidCardNumber: false})
        } else {
            setValidation({...validation, invalidCardNumber: true})
        }
    }

    // const checkInput = (value, target) => {
    //     if (target === 'cardName' && value !=='')
    //     {
    //         setValidation({...validation, invalidName: false})
    //     } else {
    //         setValidation({...validation, invalidName: true})
    //     }
    //
    //     if (target === 'cardNumber' && value
    //         .match('^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\\d{3})\\d{11})$')) {
    //         setValidation({...validation, invalidCardNumber: false})
    //     } else {
    //         setValidation({...validation, invalidCardNumber: true})
    //     }
    //
    //     if (target === 'expDate' && value.length ===5) {
    //         setValidation({...validation, invalidExpDate: false})
    //     } else {
    //         setValidation({...validation, invalidExpDate: true})
    //     }
    //
    //     if (target === 'cvv' && value.length ===3) {
    //         setValidation({...validation, invalidCvv: false})
    //     } else {
    //         setValidation({...validation, invalidCvv: true})
    //     }
    //
    // }

    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom>
                Payment method
            </Typography>
            <Grid container spacing={3}>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        id="cardName"
                        label="Name on card"
                        type='text'
                        fullWidth
                        value={payment.cardName}
                        error={payment.cardName===''}
                        helperText={payment.cardName==='' ? "cardName is required" : ''}
                        onChange={handleChange}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField required
                               id="cardNumber"
                               type='number'
                               label="Card number"
                               fullWidth
                               value={payment.cardNumber}
                               error={validation.invalidCardNumber}
                               helperText={validation.invalidCardNumber === true ? "invalid card number" : ''}
                               onChange={handleChange}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField required id="expDate"
                               label="Expire date mm/yy"
                               type='text'
                               fullWidth
                               value={payment.expDate}
                               error={payment.expDate.length!==5}
                               helperText={payment.expDate.length!==5 ? "invalid Expire Date" : ''}
                               onChange={handleChange}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        id="cvv"
                        label="CVV"
                        type='number'
                        fullWidth
                        value={payment.cvv}
                        error={payment.cvv.length!==3}
                        helperText={payment.cvv.length!==3 ? "invalid CVV" : ''}
                        onChange={handleChange}
                    />
                </Grid>
                {/*<Grid item xs={12}>*/}
                {/*    <FormControlLabel*/}
                {/*        control={<Checkbox color="secondary" name="saveCard" value="yes" />}*/}
                {/*        label="Remember credit card details for next time"*/}
                {/*    />*/}
                {/*</Grid>*/}
            </Grid>
        </React.Fragment>
    );
}
