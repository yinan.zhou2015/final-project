import React, {useState} from "react";
import {read} from "./getProductByid";
import {addItem} from "./cartHelper";
import 'bootstrap/dist/css/bootstrap.min.css';
import Col from "react-bootstrap/Col";
import {showSuccess} from "../components/Alert";
import {useDispatch} from "react-redux";
import {isAuth} from "../actions/auth.action";
import './ProductDetail.css';

const ProductDetail = props =>{
    useState(() => {
        const Id = props.match.params.id;
        const handleSubmit = (Id) => {
            read(Id).then(res => {
                setProduct(res);
            })
        }
        handleSubmit(Id);

    })
    const [product,setProduct] = useState({});
    const [success,setSuccess]=useState(false);
    const [isUser,setUser]=useState(true);

    const dispatch =useDispatch();

    dispatch(isAuth())
        .then(res => {
            //console.log('login_status:',res.payload.login_status);
            if(res.payload.login_status){
                const userProfile=res.payload.userProfile;
                //console.log(userProfile);
                return userProfile.authority === 'role_admin' ?
                    setUser(false) : setUser(true);
            }
        })


    const addToCart = () => {
        addItem(product,() => {})
        setSuccess(true);
    }




    return (

       <div
           style={{height:"700px",width:"100%"}}
           className="container"
       >
           {showSuccess({
               success:success,
               message:'Successfully added to cart, Please check your cart'
           })}
           <div className="card rounded float-left">
                 <div className="card-header name">{product.name}</div>
                 <Col>
                     <img src={product.image}
                          style={{height:"400px",width:"400px"}}
                     />
                 </Col>
             </div>

             <div className="rounded float-right">
                 <div className="card-header name">{product.name}</div>
                 <div className="card-body"
                      style={{height:"350px",width:"350px"}}
                 >
                     <p className="card-text">Category:{product.category}</p>
                     <p className="card-text"> Platform:{product.platform}</p>
                     <p className="card-text">Stock:{product.stock} Left</p>
                     <p>price:${product.price}</p >
                 </div>
                 <button
                     className="btn btn-danger"
                     onClick={addToCart}
                     disabled={!isUser || product.stock==0}
                 >Add To Cart
                 </button>
             </div>
         </div>
    );
}

export default ProductDetail;
