import React, {Component} from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import {connect} from "react-redux";
import { Link} from "react-router-dom";
import {getProducts} from "../actions/products.action";
import './Product.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import {isAuth} from "../actions/auth.action";
import FormLabel from "@material-ui/core/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import FormControl from "@material-ui/core/FormControl";
import {doFilter, doSearch} from "./filter";
import {Pagination} from "../components/Pagination";

//let isAdmin = false;

class Products extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            originalData:[],
            value: '',
            start:0,
            end:4,
            isAdmin:false,
        };
        isAuth().payload
            .then(res => {
                //console.log('login_status:',res.payload.login_status);
                if(res.login_status)
                {
                    const userProfile=res.userProfile;
                    //console.log(userProfile);

                    return userProfile.authority === 'role_admin' ?
                        this.isAdmin = true : this.isAdmin = false;
                }
            });

    }

    componentWillMount() {

        this.setState({...this.state,value:null});

            this.props.getProducts().then(res => {

                this.setState({data:res.payload.data,originalData:res.payload.data,value:null})

            })
        }

        componentDidMount() {
            !this.props.products && this.props.getProducts();


            // isAuth()
            //     .then(res => {
            //         //console.log('login_status:',res.payload.login_status);
            //         if(res.payload.login_status)
            //         {
            //             const userProfile=res.payload.userProfile;
            //             //console.log(userProfile);
            //             return userProfile.authority === 'role_admin' ?
            //                 this.setState({isAdmin:true}) : this.setState({isAdmin:false});
            //         } });
        }

        renderEdit (isAdmin,product) {
            if(isAdmin){
                return (
                    <Link to={`/edit-product/${product.id}`} className="mr-2">
                        <button className="btn btn-outline-dark">Edit Product</button>
                    </Link>
                );
            }
        }

        handleChange = (event) => {
            //console.log(this.state)
            const filtered=doFilter(this.state.originalData,event.target.value);
            //console.log(filtered)
            this.setState({...this.state,data:filtered,value: event.target.value});
        };

        handleSearch = (event) => {
            //console.log(event.target.value);
            const newProducts =doSearch(this.state.originalData,event.target.value);
            //console.log(newProducts);
            this.setState({...this.state,data:newProducts})
        }

        product() {

            return (

                <Grid container spacing = {10} style={{marginTop: '2%'}}>
                    {
                        this.state.data.slice(this.state.start, this.state.end).map(p =>
                            <Grid item xs={12} sm={6} md={4} lg={3} key={p.id}>
                                <Paper>
                                    <img src={p.image} alt={p.name} className="card-img-top"/>
                                    <div className="card-body">
                                        <div align-items-center="true">
                                            <h3 style={{margin: 0}} className="card-title">{p.name}</h3>
                                            <div className="card-text">Platform: {p.platform}</div>
                                            <div className="card-text">Stock {p.stock} Left</div>
                                            <div className="card-text">Price: ${p.price}</div>
                                            <Link to={`/product/${p.id}`} className="mr-2">
                                                <button className="btn btn-outline-dark">view detail</button>
                                            </Link>
                                            {this.renderEdit(this.isAdmin, p)}
                                        </div>
                                    </div>
                                </Paper>
                            </Grid>
                        )
                    }
                </Grid>
            )
        }

        paginate= (pageNumber) => {
            this.setState({...this.state,start:(pageNumber-1)*4 ,end:pageNumber*4})
        }

        render() {
               return(
                   <div className="row">
                       {/*<div className="col-1"></div>*/}
                       <div className="col-2">
                           <FormControl component="fieldset">
                               <FormLabel component="legend">Filter</FormLabel>
                               <RadioGroup aria-label="filter" name="filter" value={this.value} onChange={this.handleChange}>
                                   <FormControlLabel value="playstation 4" control={<Radio />} label="PS4"/>
                                   <FormControlLabel value="playstation 3" control={<Radio />} label="PS3"/>
                                   <FormControlLabel value="SWITCH" control={<Radio />} label="SWITCH"/>
                                   <FormControlLabel value="XBOX ONE" control={<Radio />} label="XBOX ONE"/>
                                   <FormControlLabel value="PC" control={<Radio />} label="PC"/>
                                   <FormControlLabel value="other" control={<Radio />} label="Other"/>
                               </RadioGroup>
                           </FormControl>
                           <div className="active-purple-4 mb-4">
                               <input
                                   className="form-control"
                                   type="text"
                                   placeholder="Search"
                                   aria-label="Search"
                                   onChange={this.handleSearch}
                               />
                           </div>
                       </div>
                       <div className="col-10 ">
                           {this.product()}
                           <Pagination postPerPage={4} totalPosts={this.state.data.length} paginate={this.paginate}/>
                       </div>
                   </div>
               )
            }
}

function mapStateToProps({products}) {
    return {products};
}


export default connect(mapStateToProps,{getProducts})(Products);


