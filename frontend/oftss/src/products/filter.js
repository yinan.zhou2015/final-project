// const doFilter = (item, filter) => {
//     let { value } = filter;
//
//     if (!(value instanceof RegExp)) {
//         value = filter.value = new RegExp(value, 'i');
//     }
//
//     return value.test(item[ filter.property ]);
// }
//
// const createFilter = (...filters) => {
//     if (typeof filters[0] === 'string') {
//         filters = [
//             {
//                 property: filters[0],
//                 value: filters[1]
//             }
//         ];
//     }
//
//     return item => filters.every(filter => doFilter(item, filter));
// };
//
// export { createFilter };
export const doFilter=(products,filter)=>{
    return filter ==='other'?
        products: products.filter(ele=>ele.platform.toLowerCase()===filter.toLowerCase()) ;
}

export const doSearch =(products,content) => {
    const newProducts = products.filter(ele => {
        //console.log(ele.name.includes(content))
        return ele.name.toLowerCase().includes(content.toLowerCase())
            || ele.platform.toLowerCase().includes(content.toLowerCase())
            || ele.category.toLowerCase().includes(content.toLowerCase())
    });
    //console.log(newProducts);
    return newProducts;
}
