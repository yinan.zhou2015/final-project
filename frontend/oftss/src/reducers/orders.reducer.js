import {appConstants} from "../constants/constants";

export const ordersReducer =(state=null, action) => {
    switch (action.type)
    {
        case appConstants.GET_ALL_PURCHASES:
            console.log(action.payload);
            return action.payload.orders;
        case appConstants.GET_SHIPPING_INFO:
            return action.payload.shippingInfo;
        default:
            return state;
    }
}
