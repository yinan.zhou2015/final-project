import {appConstants} from "../constants/constants";

export const productsReducer =(state=null, action) => {
    switch (action.type)
    {
        case appConstants.GET_PRODUCTS:
            return action.payload.data;

        case appConstants.ADD_PRODUCT:
            return action.payload.success && state ? [...state,action.payload.product] : state;

        case appConstants.GET_STOCK_TO_CHECK:
            //console.log(action.payload.data);
            // return action.payload.success && state ? [...state,action.payload.data] : state;
            return action.payload.data;

        default:
            return state;
    }
}
