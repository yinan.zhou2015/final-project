import React from "react";
import {changePassword} from "../actions/auth.action";
import {showError, showSuccess} from "../components/Alert";
import Layout from "../components/Layout";
import 'bootstrap/dist/css/bootstrap.min.css';

export const ChangePassword = () => {

    const [user, setUser] = React.useState({
        username: '',
        password: '',
        confirmed: '',
        success: false,
        error: false,
    });


    const handleSubmit = (event) => {
        event.preventDefault();
        // const matched=(user.password===user.confirmed);
        //console.log(user.password === user.confirmed);
        if (user.password === user.confirmed) {
            //console.log("matched")
            const newUser = {
                username: user.username,
                password: user.password
            }
            changePassword(newUser);
            setUser({...user, success: true, error: false});
        } else {
            //console.log("not matched")
            setUser({...user, success: false, error: true});
        }

    }


    const handleNewPasswordChange = (event) => {
        const localUser = localStorage.getItem("user");
        const userNa = JSON.parse(localUser).username;
        //console.log(userNa)
        setUser({...user, username: userNa, password: event.target.value});
    }

    const handleConfirmedPasswordChange = (event) => {
        setUser({...user, confirmed: event.target.value});
    }

    const changePasswordForm = () => {

        return (
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <label className="text-muted">New Password</label>
                    <input onChange={handleNewPasswordChange}
                           type="password" required
                           placeholder='New Password'
                           className="form-control"
                           //value={user.username}
                    />
                </div>

                <div className="form-group">
                    <label className="text-muted">Confirm Password</label>
                    <input onChange={handleConfirmedPasswordChange}
                           type="password" required
                           placeholder='Confirmed Password'
                           className="form-control"
                           //value={user.password}
                    />
                </div>
                <button  className="btn-primary">Reset Password</button>
            </form>
        )
    };

    return (

        <Layout
            title="Change Password"
            classname="container col-md-8 offset-md-2"
        >
            {showSuccess({
                        success: user.success,
                        message: "Successfully reset password, Your can logout to check"
                    })}
            {showError(
                {
                        error: user.error,
                        message: "Password doesn't match"
                    }
            )}
            {changePasswordForm()}
            {/*{JSON.stringify(user)}*/}
        </Layout>
    )
}
