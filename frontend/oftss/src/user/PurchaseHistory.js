import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {cancelOrders, getOrderByUserId, updateOrder} from "../actions/order.action";

export const PurchaseHistory =() => {

    const [history,setHistory] = useState({
            purchases:[],
            error: false,
            success:true,
            cancel:false,
            cancelButtons:[],
        }
    );

    const dispatch=useDispatch();

    const userId=JSON.parse(localStorage.getItem("ud")).id;
    //console.log(userId);
    const historyPromise=dispatch(getOrderByUserId(userId));
    historyPromise.then(res => {
        //console.log(res);
        if(history.success){
            //console.log(res.payload.orders)
            setHistory({...history,purchases:res.payload.orders,success:false});
        }
    });

    const handleCancel =(e) =>{
        //console.log(e.target.value);
        e.preventDefault();
        const order={
            id:e.target.value,
            status:"processing"
        }

        updateOrder(order);
        const newPurchases=[];
        const buttons=[];
        history.purchases.forEach(purchase=>{
            if(purchase.order_id==e.target.value) {
                purchase.status=order.status;
            }
            newPurchases.push(purchase);
            if(purchase.status.toLowerCase()==="processing"){
                buttons.push(purchase.order_id);
            }
        });
        setHistory({...history,purchases:newPurchases});
        //console.log(buttons);
        //cancelOrders(buttons.join());
        //console.log(history.cancelButtons.join());
    }


    const handleProcess =(e)=> {
        const order={
            id:e.target.value,
            status:"pending"
        };

        updateOrder(order);
        const newPurchases=[];
        history.purchases.forEach(purchase=>{
            if(purchase.order_id==e.target.value) {
                purchase.status=order.status;
            }
            newPurchases.push(purchase);
        })
        //const buttons=history.cancelButtons.filter(ele => ele !== e.target.value);
        setHistory({...history,purchases:newPurchases});
    }

    const renderButton =(key,status) => {
        //console.log(status);
        return status.toLowerCase() === "processing"?
                <button
                    value={key}
                    disabled={true}
                    onClick={ (event) => {
                    handleProcess(event)
                }}>Processing</button>
                :
                <button
                    value={key}
                    onClick={(event) => {
                    handleCancel(event);
                }}
                    hidden={status==="cancelled" || status==='shipped'}
                >Cancel</button>
    }

    const itemTotal=(price,qty)=>{
        return price*qty;
    }

    const renderStatus=(status) => {
        return status==='processing'? 'Cancel Processing' :status;
    }

    const purchaseHistory = history => {
        return (
            <div className="card mb-5">
                <h3 className="card-header">Purchase history</h3>
                <ul className="list-group">
                    <li className="list-group-item">
                        {history.purchases.map((h, i) => {
                            return (
                                <div key={i}>
                                    <hr />
                                    {h.purchases.map((p, i) => {
                                        return (
                                            <div key={i}>
                                                <img src={p.product.image} style={{
                                                    height:'50px',
                                                    width:'50px',
                                                }}/>
                                                <h6>Item name: {p.product.name}</h6>
                                                <h6>Qty:{p.qty}</h6>
                                                <h6>
                                                    Item total price: $ {itemTotal(p.product.price,p.qty)}
                                                </h6>
                                                <h6>
                                                    Purchased date: {h.purchase_date}
                                                </h6>
                                            </div>
                                        );
                                    })}
                                    <h6>Order Status: {renderStatus(h.status)}</h6>
                                    {renderButton(h.order_id,h.status)}
                                </div>

                            );
                        })}
                    </li>
                </ul>
            </div>
        );
    };

    return (
        <div>
            {purchaseHistory(history)}
        </div>
    );
}

